package me.duelba.gui;

import me.duelba.main.Main;
import me.duelba.mysql.StockSQL;
import me.duelba.stock.StockHandler;
import me.duelba.utils.FormatValueUtility;
import me.duelba.utils.SkullBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Portfolio {

    private String uuid;
    private StockHandler stockHandler;
    private FormatValueUtility format;
    private Inventory inv;
    private int amountOfStocks;
    private int amountOfPages;
    private int currentPage = 1;

    public Portfolio(String uuid) {
        this.uuid = uuid;
        this.stockHandler = new StockHandler();
        this.format = new FormatValueUtility();
    }

    public Inventory getInv(int absoluteRow) {
        inv = Bukkit.createInventory(null, 27, "§6§lPortfolio");
        ResultSet result = StockSQL.getAllStocksByUUID(uuid);
        int counter = 0;
        try {
            result.last();
            int tmpCountStocks = result.getRow();
            result.absolute(absoluteRow);
            while (result.next()) {
                String stockId = result.getString("id");
                String stockStartValue = result.getString("stockStartValue");
                String stockKey = result.getString("stockKey");
                String amount = result.getString("amount");
                String stockName = stockHandler.getNameByKey(stockKey);
                String stockDisplayName = stockHandler.getDisplayNameByName(stockName);
                String currentValue = stockHandler.getValueByKey(stockKey);

                ItemStack item = new ItemStack(Material.BOOK, Integer.parseInt(amount));
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§9" + stockDisplayName);
                List<String> lore = new ArrayList<>();
                lore.add("§7Share amount: §e" + format.addThousandSeparatorToInt(amount));
                lore.add("§7Purchase value: §6" + format.formatDoubleToString(Double.parseDouble(stockStartValue)) + "$");
                lore.add("§7Current value: §6" + format.formatDoubleToString(Double.parseDouble(currentValue)) + "$");
                lore.add("§7Your profit: " + getProfit(currentValue, stockStartValue, Integer.parseInt(amount)) + "$");

                lore.add(convertToInvisibleString(stockId));
                lore.add("§aPress left click to sell a share.");
                lore.add("§aPress right click to sell all shares.");
                meta.setLore(lore);
                item.setItemMeta(meta);
                inv.setItem(counter, item);
                counter++;
                this.amountOfStocks = tmpCountStocks;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return inv;
    }

    private String getProfit(String currentValue, String stockStartValue, int amount) {
        return getChatColor(currentValue, stockStartValue)
                + getSymbolIfPlus(currentValue, stockStartValue)
                + format.formatDoubleToString(amount * (Double.parseDouble(currentValue) - Double.parseDouble(stockStartValue)));
    }

    private String getChatColor(String current, String invest) {
        if(Double.parseDouble(current) >= Double.parseDouble(invest)) {
            return "§a";
        }
        return "§c";
    }

    private String getSymbolIfPlus(String current, String invest) {
        if(Double.parseDouble(current) >= Double.parseDouble(invest)) {
            return "+";
        }
        return "";
    }

    private String convertToInvisibleString(String s) {
        String hidden = "";
        for (char c : s.toCharArray()) hidden += ChatColor.COLOR_CHAR+""+c;
        return hidden;
    }

    public int getAmountOfStocks() {
        return amountOfStocks;
    }

    public int getAmountOfPages() {
        return amountOfPages;
    }

    public void setAmountOfPages(int amountOfPages) {
        this.amountOfPages = amountOfPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}
