package me.duelba.gui;

import me.duelba.mysql.StockSQL;
import me.duelba.stock.StockHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Confirm {

    private Inventory inv;
    private String title;
    private String stockIdToSell;

    public Confirm(String stockIdToSell, String title) {
        this.stockIdToSell = stockIdToSell;
        this.title = title;
    }

    public Inventory showConfirmSaleInventory() {
        inv = Bukkit.createInventory(null, 9, "§6§l" + title);

        ItemStack emerald = new ItemStack(Material.EMERALD_BLOCK, 1);
        ItemStack redstone = new ItemStack(Material.REDSTONE_BLOCK, 1);

        ItemMeta emeraldMeta = emerald.getItemMeta();
        ItemMeta redstoneMeta = redstone.getItemMeta();

        List<String> lore = new ArrayList<>();
        lore.add(convertToInvisibleString(stockIdToSell));

        emeraldMeta.setDisplayName("§aConfirm Sale");
        emeraldMeta.setLore(lore);
        redstoneMeta.setDisplayName("§cAbort Sale");

        emerald.setItemMeta(emeraldMeta);
        redstone.setItemMeta(redstoneMeta);
        inv.setItem(0, redstone);
        inv.setItem(8, emerald);

        return inv;
    }

    private String convertToInvisibleString(String s) {
        String hidden = "";
        for (char c : s.toCharArray()) hidden += ChatColor.COLOR_CHAR+""+c;
        return hidden;
    }
}
