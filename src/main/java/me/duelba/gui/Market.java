package me.duelba.gui;

import me.duelba.stock.Stock;
import me.duelba.stock.StockHandler;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class Market {

    private String uuid;
    private StockHandler stockHandler;
    private Inventory inv;

    public Market(String uuid) {
        this.uuid = uuid;
        this.stockHandler = new StockHandler();
    }

    public Inventory getInv() {
        inv = Bukkit.createInventory(null, 27, "§6§Market");
        HashMap<String, Stock> availableStocks = stockHandler.getAvailableStocks();
        return inv;
    }

}
