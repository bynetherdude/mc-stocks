package me.duelba.main;

import me.duelba.commands.*;
import me.duelba.events.InventoryListener;
import me.duelba.events.JoinListener;
import me.duelba.gui.Portfolio;
import me.duelba.mysql.MySQL;
import me.duelba.mysql.StockSQL;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public final class Main extends JavaPlugin {

    private static final String prefix = "§cMcStocks §8» §6";
    private static final File mysqlFile = new File("plugins//McStocks//mysql.yml");
    private static final File configFile = new File("plugins//McStocks//config.yml");
    private static final YamlConfiguration mysql = YamlConfiguration.loadConfiguration(mysqlFile);
    private static final YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
    private static final HashMap<String, Portfolio> portFolioList = new HashMap<>();

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage(getPrefix() + "§aPlugin started!");

        registerCommands();
        registerEvents();

        loadConfig();

        MySQL.connect();
        StockSQL.initializeSQL();
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(getPrefix() + "§cPlugin stopped!");

        MySQL.disconnect();
    }

    private void loadConfig() {
        mysql.addDefault("mysql.host", "host");
        mysql.addDefault("mysql.database", "database");
        mysql.addDefault("mysql.port", "3306");
        mysql.addDefault("mysql.username", "root");
        mysql.addDefault("mysql.password", "");

        config.addDefault("general.startBalance", 1000.0);

        config.addDefault("permissions.seeOthersMoney", "mcstocks.money");
        config.addDefault("permissions.setMoney", "mcstocks.setmoney");

        mysql.options().copyDefaults(true);
        config.options().copyDefaults(true);
        try {
            mysql.save(mysqlFile);
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void registerCommands() {
        getCommand("stock").setExecutor(new StockCommand());
        getCommand("money").setExecutor(new MoneyCommand());
        getCommand("moneytop").setExecutor(new MoneyTopCommand());
        getCommand("pay").setExecutor(new PayCommand());
        getCommand("setmoney").setExecutor(new SetMoneyCommand());
    }

    private void registerEvents() {
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new InventoryListener(), this);
    }

    public static String getPrefix() {
        return prefix;
    }

    public static YamlConfiguration getMySQLYaml() {
        return mysql;
    }

    public static YamlConfiguration getConfigYaml() {
        return config;
    }

    public static HashMap<String, Portfolio> getPortFolioList() {
        return portFolioList;
    }
}
