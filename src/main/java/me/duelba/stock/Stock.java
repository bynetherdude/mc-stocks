package me.duelba.stock;

public class Stock {

    private final String displayName;
    private final String key;
    private final boolean isCrypto;

    public Stock(String displayName, String key) {
        this.displayName = displayName;
        this.key = key;
        this.isCrypto = false;
    }

    public Stock(String displayName, String key, boolean isCrypto) {
        this.displayName = displayName;
        this.key = key;
        this.isCrypto = isCrypto;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getKey() {
        return key;
    }
}
