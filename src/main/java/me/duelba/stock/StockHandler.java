package me.duelba.stock;

import me.duelba.main.Main;
import me.duelba.mysql.StockSQL;
import me.duelba.utils.FormatValueUtility;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class StockHandler {
    private static HashMap<String, Stock> availableStocks;
    private FormatValueUtility format;

    public StockHandler() {
        availableStocks = new HashMap<>();
        format = new FormatValueUtility();
        registerStocks();
    }

    public HashMap<String, Stock> getAvailableStocks() {
        return availableStocks;
    }

    public String getKeyByName(String name) {
        Stock stock = availableStocks.get(name.toLowerCase());
        return stock.getKey();
    }

    public String getNameByKey(String key) {
        Iterator hmIterator = availableStocks.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry) hmIterator.next();
            Stock s = (Stock) mapElement.getValue();
            if(key.equals(s.getKey())) {
                return mapElement.getKey().toString();
            }
        }
        return null;
    }

    public String getDisplayNameByName(String name) {
        Stock stock = availableStocks.get(name.toLowerCase());
        return stock.getDisplayName();
    }

    public String getValueByKey(String key) {
        try {
            String value = String.valueOf(YahooFinance.get(key).getQuote().getPrice());
            value = String.format("%.5f", Double.parseDouble(value));
            if(value != null) {
                // TODO: Tausender Trennzeichen
                return value;
            } else {
                Bukkit.broadcastMessage("§cStock not found!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void buyStock(String identifier, String amount, Player p) {
        String key = getKeyByName(identifier);
        double stockStartValue = Double.parseDouble(getValueByKey(key));
        double playerBalance = StockSQL.getBalanceByUUID(p.getUniqueId().toString());

        if(stockStartValue * Integer.parseInt(amount) < playerBalance) {
            StockSQL.buyStock(p.getUniqueId().toString(), amount, p.getName(), key, stockStartValue);
            StockSQL.updateBalanceOfUser(p.getUniqueId().toString(), playerBalance - (stockStartValue * Integer.parseInt(amount)));
            p.sendMessage(Main.getPrefix() + "§aYou successfully purchased §6" + format.addThousandSeparatorNoComma(amount) + " " + getDisplayNameByName(identifier) + " §6(" + format.addThousandSeparator(stockStartValue * Integer.parseInt(amount)) + "$) §ashare(s)!");
        } else {
            p.sendMessage(Main.getPrefix() + "§cYou don't have enough money for that.");
        }
    }

    public void sellOneShare(String shareId, Player p) {
        ResultSet rs = StockSQL.getStockByStockId(shareId);
        boolean isShareOwner = StockSQL.checkIfUserOwnsStockByItsId(p.getUniqueId().toString(), shareId);
        if(isShareOwner) {
            try {
                if(rs.next()) {
                    try {
                        rs.first();
                        double startStockValue = Double.parseDouble(rs.getString("stockStartValue"));
                        int amount = Integer.parseInt(rs.getString("amount"));
                        String key = rs.getString("stockKey");
                        double currentShareValue = Double.parseDouble(getValueByKey(key));
                        double differenceShareValue = currentShareValue - startStockValue;
                        double playerBalance = StockSQL.getBalanceByUUID(p.getUniqueId().toString());
                        StockSQL.updateBalanceOfUser(p.getUniqueId().toString(), playerBalance + currentShareValue);
                        if(amount > 1) {
                            StockSQL.updateAmount(shareId, String.valueOf(amount - 1));
                        } else {
                            StockSQL.sellShare(shareId);
                        }
                        if(differenceShareValue >= 0) {
                            p.sendMessage(Main.getPrefix()
                                    + "§aYou successfully sold one §6" + key + " §ashare for §6" + format.addThousandSeparator(currentShareValue) + "$ §2(+" + format.addThousandSeparator(differenceShareValue) + ")");
                        } else {
                            p.sendMessage(Main.getPrefix() + "§aYou successfully sold one §6" + key + " §ashare for §6" + format.addThousandSeparator(currentShareValue) + "$ §c(+" + format.addThousandSeparator(differenceShareValue) + ")");
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            p.sendMessage(Main.getPrefix() + "§cAn unexpected error occurred while selling your stock!");
        }
    }

    public void sellAllShares(String shareId, Player p) {
        ResultSet rs = StockSQL.getStockByStockId(shareId);
        boolean isShareOwner = StockSQL.checkIfUserOwnsStockByItsId(p.getUniqueId().toString(), shareId);
        if(isShareOwner) {
            try {
                if(rs.next()) {
                    try {
                        rs.first();
                        double startStockValue = Double.parseDouble(rs.getString("stockStartValue"));
                        int amount = Integer.parseInt(rs.getString("amount"));
                        String key = rs.getString("stockKey");
                        double currentShareValue = Double.parseDouble(getValueByKey(key));
                        double differenceShareValue = currentShareValue - startStockValue;
                        double playerBalance = StockSQL.getBalanceByUUID(p.getUniqueId().toString());
                        StockSQL.updateBalanceOfUser(p.getUniqueId().toString(), playerBalance + (currentShareValue * amount ));
                        StockSQL.sellShare(shareId);
                        if(differenceShareValue >= 0) {
                            p.sendMessage(Main.getPrefix()
                                    + "§aYou successfully sold §6" + format.addThousandSeparatorToInt(String.valueOf(amount)) + " " + key + " §ashares for §6" + format.addThousandSeparator(currentShareValue * amount) + "$ §2(+" + format.addThousandSeparator(differenceShareValue * amount) + ")");
                        } else {
                            p.sendMessage(Main.getPrefix() + "§aYou successfully sold a §6" + format.addThousandSeparatorToInt(String.valueOf(amount)) + " " + key + " §ashare for §6" + format.addThousandSeparator(currentShareValue) + "$ §c(" + format.addThousandSeparator(differenceShareValue * amount) + ")");
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            p.sendMessage(Main.getPrefix() + "§cAn unexpected error occurred while selling your stock!");
        }
    }

    private void registerStocks() {
        availableStocks.put("bitcoin", new Stock("Bitcoin", "BTC-USD" ,true));
        availableStocks.put("dogecoin", new Stock("Dogecoin", "DOGE-USD" ,true));

        availableStocks.put("gamestop", new Stock("Gamestop", "GME"));
        availableStocks.put("apple", new Stock("Apple Inc.", "AAPL"));
        availableStocks.put("tesla", new Stock("Tesla", "TSLA"));
        availableStocks.put("clovis", new Stock("Clovis", "CLVS"));
        availableStocks.put("sundial", new Stock("Sundial Growers", "SNDL"));
        availableStocks.put("hall-of-fame-resort", new Stock("Hall of Fame Resort", "HOFV"));
        availableStocks.put("amc-entertainment", new Stock("Amc Entertainment", "AMC"));
        availableStocks.put("supercom", new Stock("Supercom LTD", "SPCB"));
        availableStocks.put("proshares", new Stock("Proshares Trust QQQ", "SQQQ"));
        availableStocks.put("spdr", new Stock("SPDR S&P 500", "SPY"));
        availableStocks.put("zomedica", new Stock("Zomedica Corp.", "ZOM"));
        availableStocks.put("bank-of-america", new Stock("Bank of America", "ZOM"));
        availableStocks.put("nio", new Stock("NIO Inc.", "NIO"));
        availableStocks.put("ford", new Stock("Ford", "F"));
        availableStocks.put("zk", new Stock("ZK International", "ZKIN"));
        availableStocks.put("exxon", new Stock("Exxon Mobil Corporation", "XOM"));
        availableStocks.put("invesco", new Stock("Invesco QQQ", "QQQ"));
        availableStocks.put("senestech", new Stock("Senestech", "SNES"));
        availableStocks.put("coca-cola", new Stock("Coca-Cola Co.", "KO"));
        availableStocks.put("skillz", new Stock("Skillz Inc.", "SKLZ"));
        availableStocks.put("cisco", new Stock("Cisco Systems", "CSCO"));
        availableStocks.put("intel", new Stock("Intel Corp.", "INTC"));
        availableStocks.put("jpmorgan", new Stock("JPMorgan", "JPM"));
        availableStocks.put("hp", new Stock("HP INC", "HPQ"));
        availableStocks.put("nokia", new Stock("Nokia", "NOK"));
        availableStocks.put("microsoft", new Stock("Microsoft Corp.", "MSFT"));
        availableStocks.put("amd", new Stock("AMD Inc.", "AMD"));
        availableStocks.put("keycorp", new Stock("Keycorp", "KEY"));
        availableStocks.put("oracle", new Stock("Oracle Corp. ", "ORCL"));
        availableStocks.put("walt-disney", new Stock("Walt Disney", "DIS"));
        availableStocks.put("beyondmeat", new Stock("Beyond Meat", "BYND"));
    }
}
