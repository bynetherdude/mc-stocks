package me.duelba.mysql;

import me.duelba.main.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StockSQL {

    public static void initializeSQL() {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS User(id int(11) PRIMARY KEY AUTO_INCREMENT, uuid varchar(100), username varchar(100), balance double(100,5))");
            PreparedStatement ps1 = MySQL.getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS Stock(id int(11) PRIMARY KEY AUTO_INCREMENT, uuid varchar(100), amount int(11), username varchar(100), stockKey varchar(100), stockStartValue double(100,5))");
            ps.execute();
            ps1.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertUser(String uuid, String name) {
        if(isNotRegistered(uuid)) {
            try {
                PreparedStatement ps = MySQL.getConnection().prepareStatement("INSERT INTO User(uuid, username, balance) VALUES (?, ?, " + Main.getConfigYaml().getDouble("general.startBalance") + ")");
                ps.setString(1, uuid);
                ps.setString(2, name);
                ps.execute();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static double getBalanceByUUID(String uuid) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT balance FROM User WHERE uuid LIKE ?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return rs.getDouble("balance");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public static ResultSet getAllStocksByUUID(String uuid) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT * FROM Stock WHERE uuid LIKE ?");
            ps.setString(1, uuid);
            return ps.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void updateBalanceOfUser(String uuid, double balance) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("UPDATE User SET balance = ? WHERE uuid = ?");
            ps.setString(1, String.valueOf(balance));
            ps.setString(2, uuid);
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static ResultSet getMoneyTop() {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT * FROM User ORDER BY balance DESC LIMIT 5");
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static int getRowCountOfUsers() {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT count(*) AS rowcount FROM User");
            ResultSet rs = ps.executeQuery();
            if(rs.last()) {
                return rs.getRow();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public static void buyStock(String uuid, String amount, String username, String stockKey, double stockStartValue) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("INSERT INTO Stock(uuid, amount, username, stockKey, stockStartValue) VALUES (?, ?, ?, ?, ?)");
            ps.setString(1, uuid);
            ps.setString(2, amount);
            ps.setString(3, username);
            ps.setString(4, stockKey);
            ps.setString(5, String.valueOf(stockStartValue));
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static boolean isNotRegistered(String uuid) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT * FROM User WHERE uuid LIKE ?");
            ps.setString(1, uuid);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return rs == null;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    public static void sellShare(String shareId) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("DELETE FROM Stock WHERE id = ?");
            ps.setString(1, shareId);
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateAmount(String shareId, String amount) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("UPDATE Stock SET amount = ? WHERE id = ?");
            ps.setString(1, amount);
            ps.setString(2, shareId);
            ps.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static ResultSet getStockByStockId(String stockId) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT * FROM Stock WHERE id = ?");
            ps.setString(1, stockId);
            return ps.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static boolean checkIfUserOwnsStockByItsId(String givenUUID, String stockId) {
        try {
            PreparedStatement ps = MySQL.getConnection().prepareStatement("SELECT * FROM Stock WHERE id = ? AND uuid = ?");
            ps.setString(1, stockId);
            ps.setString(2, givenUUID);
            ResultSet rs = ps.executeQuery();
            rs.next();
            String uuidFromSQL = rs.getString("uuid");
            if(givenUUID.equals(uuidFromSQL)) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
