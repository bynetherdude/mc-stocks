package me.duelba.mysql;

import me.duelba.main.Main;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

    private static final String hostname = (String) Main.getMySQLYaml().get("mysql.host");
    private static final String database = (String) Main.getMySQLYaml().get("mysql.database");
    private static final String port = (String) Main.getMySQLYaml().get("mysql.port");
    private static final String username = (String) Main.getMySQLYaml().get("mysql.username");
    private static final String password = (String) Main.getMySQLYaml().get("mysql.password");

    public static Connection connection;

    public static void connect() {
        if(!isConnected()) {
            try {
                Bukkit.getConsoleSender().sendMessage("host: " + hostname);
                connection = DriverManager.getConnection("jdbc:mysql://" + hostname + ":" + port + "/" + database, username, password);
                Bukkit.getConsoleSender().sendMessage(Main.getPrefix() + "§aMySQL Connection established!");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                Bukkit.getConsoleSender().sendMessage(Main.getPrefix() + "§cMake sure to update your mysql.yml file. (Enter your mysql login)");
            }
        }
    }

    public static void disconnect() {
        if(isConnected()) {
            try {
                connection.close();
            } catch(SQLException throwables) {
                throwables.printStackTrace();
            }
            Bukkit.getConsoleSender().sendMessage(Main.getPrefix() + "§cMySQL Connection interrupted!");
        }
    }

    public static boolean isConnected() {
        return connection != null;
    }

    public static Connection getConnection() {
        return connection;
    }
}
