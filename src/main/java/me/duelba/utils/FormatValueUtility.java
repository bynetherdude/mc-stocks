package me.duelba.utils;

import java.text.DecimalFormat;

public class FormatValueUtility {

    public String formatDoubleToString(double amount) {
        // Round
        DecimalFormat df = new DecimalFormat("###,##0.0####");
        return df.format(amount);
    }

    /**
     * Should only be used for integers
     */
    public String addThousandSeparatorToInt(String amount) {
        DecimalFormat df = new DecimalFormat("###,###,###");
        return df.format(Double.parseDouble(amount));
    }

    /**
     * Should only be used for doubles
     * Returns i.e. 1,000.0
     */
    public String addThousandSeparator(String amount) {
        DecimalFormat df = new DecimalFormat("###,##0.0####");
        return df.format(Double.parseDouble(amount));
    }

    /**
     * Same as the method above but accepts double instead of String
     */
    public String addThousandSeparator(double amount) {
        String s = String.valueOf(amount);
        return addThousandSeparator(s);
    }

    public String addThousandSeparatorNoComma(String amount) {
        DecimalFormat df = new DecimalFormat("###,###");
        return df.format(Double.parseDouble(amount));
    }

    /**
     * Same as the method above but accepts double instead of String
     */
    public String addThousandSeparatorNoComma(double amount) {
        String s = String.valueOf(amount);
        return addThousandSeparator(s);
    }
}
