package me.duelba.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullBuilder {

    public static ItemStack getCustomSkull(String playerName) {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
        skullMeta.setOwner(playerName);
        itemStack.setItemMeta(skullMeta);
        return itemStack;
    }

    public static ItemStack getArrowLeftSkull() {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
        skullMeta.setDisplayName("§a««« Last page");
        skullMeta.setOwner("MHF_ArrowLeft");
        itemStack.setItemMeta(skullMeta);
        return itemStack;
    }

    public static ItemStack getArrowRightSkull() {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
        SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
        skullMeta.setDisplayName("§aNext page »»»");
        skullMeta.setOwner("MHF_ArrowRight");
        itemStack.setItemMeta(skullMeta);
        return itemStack;
    }
}
