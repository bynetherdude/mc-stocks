package me.duelba.commands;

import me.duelba.main.Main;
import me.duelba.mysql.StockSQL;
import me.duelba.utils.FormatValueUtility;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PayCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            FormatValueUtility format = new FormatValueUtility();
            if(args.length == 2) {
                Player target = Bukkit.getPlayer(args[0]);
                if(!args[0].equals(p.getName())) {
                    if (target.isOnline()) {
                        try {
                            double valueToTransfer = Double.parseDouble(args[1]);
                            if (valueToTransfer > 0) {
                                double senderBalance = StockSQL.getBalanceByUUID(p.getUniqueId().toString());
                                double receiverBalance = StockSQL.getBalanceByUUID(target.getUniqueId().toString());
                                if (valueToTransfer > senderBalance) {
                                    p.sendMessage(Main.getPrefix() + "§cYou don't have enough money to do that.");
                                } else {
                                    StockSQL.updateBalanceOfUser(p.getUniqueId().toString(), senderBalance - valueToTransfer);
                                    StockSQL.updateBalanceOfUser(target.getUniqueId().toString(), receiverBalance + valueToTransfer);
                                    p.sendMessage(Main.getPrefix() + "§6You successfully transfered §a" + format.addThousandSeparator(valueToTransfer) + "$ §6to " + target.getName());
                                    target.sendMessage(Main.getPrefix() + "§6You just received §a" + format.addThousandSeparator(valueToTransfer) + "$ §6from " + p.getName());
                                }
                            } else {
                                p.sendMessage(Main.getPrefix() + "§cYou have to enter a positive amount.");
                            }
                        } catch (NumberFormatException e) {
                            p.sendMessage(Main.getPrefix() + "§cThe balance has to be numeric. Usage: /pay <name> <amount>");
                        }
                    } else {
                        p.sendMessage(Main.getPrefix() + "§cThis player isn't currently online.");
                    }
                } else {
                    p.sendMessage(Main.getPrefix() + "§cYou can't transfer money to yourself.");
                }
            } else {
                p.sendMessage(Main.getPrefix() + "§cUsage: /pay <name> <amount>");
            }
        } else {
            Bukkit.getConsoleSender().sendMessage(Main.getPrefix() + "§cThis command has to be executed by a player.");
        }
        return false;
    }
}
