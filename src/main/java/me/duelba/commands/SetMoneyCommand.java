package me.duelba.commands;

import me.duelba.main.Main;
import me.duelba.mysql.StockSQL;
import me.duelba.utils.FormatValueUtility;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetMoneyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            FormatValueUtility format = new FormatValueUtility();
            if(p.hasPermission(Main.getConfigYaml().getString("permissions.setMoney"))) {
                if(args.length == 2) {
                    Player target = Bukkit.getServer().getPlayer(args[0]);
                    try {
                        double balanceToSet = Double.parseDouble(args[1]);
                        StockSQL.updateBalanceOfUser(target.getUniqueId().toString(), balanceToSet);
                        p.sendMessage(Main.getPrefix() + "§aYou successfully set §6" + target.getName() + "'s §abalance to §6" + format.addThousandSeparator(balanceToSet) + "$");
                        target.sendMessage(Main.getPrefix() + "§aYour balance has been updated to §6" + format.addThousandSeparator(balanceToSet) + "$");
                    } catch(NumberFormatException e) {
                        p.sendMessage(Main.getPrefix() + "§cUsage: /setmoney <name> <balance>");
                    }
                } else {
                    p.sendMessage(Main.getPrefix() + "§cUsage: /setmoney <name> <balance>");
                }
            } else {
                p.sendMessage(Main.getPrefix() + "§cYou don't have enough permissions to execute this command.");
            }
        }
        return false;
    }
}
