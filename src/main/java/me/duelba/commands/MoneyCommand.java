package me.duelba.commands;

import me.duelba.main.Main;
import me.duelba.mysql.StockSQL;
import me.duelba.utils.FormatValueUtility;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MoneyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            FormatValueUtility format = new FormatValueUtility();
            if(args.length == 0) {
                double balance = StockSQL.getBalanceByUUID(p.getUniqueId().toString());
                p.sendMessage(Main.getPrefix() + "§6Your balance is §a" + format.addThousandSeparator(balance) + "$");
            } else if(args.length >= 1) {
                if(p.hasPermission(Main.getConfigYaml().getString("permissions.seeOthersMoney"))) {
                    if(args.length == 1) {
                        OfflinePlayer target = Bukkit.getServer().getOfflinePlayer(args[0]);
                        double balance = StockSQL.getBalanceByUUID(target.getUniqueId().toString());
                        p.sendMessage(Main.getPrefix() + "§6The balance of §a" + target.getName() + " §6is §a" + format.addThousandSeparator(balance) + "$");
                    }
                } else {
                    p.sendMessage(Main.getPrefix() + "§cUsage: /money");
                }
            }
        } else {
            Bukkit.getConsoleSender().sendMessage(Main.getPrefix() + "§cThis command has to be executed by a player.");
        }
        return false;
    }
}
