package me.duelba.commands;

import me.duelba.gui.Portfolio;
import me.duelba.main.Main;
import me.duelba.mysql.StockSQL;
import me.duelba.stock.Stock;
import me.duelba.stock.StockHandler;
import me.duelba.utils.SkullBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class StockCommand implements CommandExecutor {

    StockHandler stockHandler = new StockHandler();
    Player p;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Main.getPrefix() + "§cThis command can only be used as a player!");
            return true;
        }
       p = (Player) sender;

        if(args.length == 1) {
            if(args[0].equalsIgnoreCase("list")) {
                showStockList();
                return true;
            }
            if(args[0].equalsIgnoreCase("portfolio")) {
                showPortfolio();
                return true;
            }
            if(args[0].equalsIgnoreCase("help")) {
                showHelp();
                return true;
            }
        }
        if(args.length == 2) {
            if (args[0].equalsIgnoreCase("price")) {
                showPrice(args[1]);
                return true;
            }
        }
        if(args.length == 3) {
            if(args[0].equalsIgnoreCase("buy")) {
                stockHandler.buyStock(args[1], args[2], p);
                return true;
            }
        }
        p.sendMessage(Main.getPrefix() + "§cIncorrect usage, please see /stock help");
        return true;
    }

    private void showPrice(String name) {
        String key = stockHandler.getKeyByName(name);
        String value = stockHandler.getValueByKey(key);
        String displayName = stockHandler.getDisplayNameByName(name);
        if(value != null && displayName != null) {
            p.sendMessage(Main.getPrefix() + displayName + " has a value of §a" + value + "$ per share.");
            return;
        }
        p.sendMessage(Main.getPrefix() + "§cThis stock is not available! Try using /stock list to find your stock.");
    }

    private void showPortfolio() {
        ResultSet rs = StockSQL.getAllStocksByUUID(p.getUniqueId().toString());
        try {
            if(!rs.next()) {
                p.sendMessage(Main.getPrefix() + "§cYou don't own any shares.");
                return;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        Portfolio pf;
        if(!Main.getPortFolioList().containsKey(p.getUniqueId().toString())) {
            Main.getPortFolioList().put(p.getUniqueId().toString(), new Portfolio(p.getUniqueId().toString()));
        }
        pf = Main.getPortFolioList().get(p.getUniqueId().toString());
        Inventory inv = pf.getInv(pf.getCurrentPage() * 17);
        double amountOfStocks = pf.getAmountOfStocks();
        int amountOfPages = Integer.parseInt(String.valueOf(Math.ceil(amountOfStocks / 18)));
        pf.setAmountOfPages(amountOfPages);

        if(pf.getCurrentPage() == 1) {
            inv.setItem(26, SkullBuilder.getArrowRightSkull());
        } else if(pf.getCurrentPage() == pf.getAmountOfPages()) {
            inv.setItem(18, SkullBuilder.getArrowLeftSkull());
        } else {
            inv.setItem(26, SkullBuilder.getArrowRightSkull());
            inv.setItem(18, SkullBuilder.getArrowLeftSkull());
        }
        p.openInventory(inv);
    }

    private void showStockList() {
        String message = "";
        message += "§9-----------------------------\n";
        HashMap<String, Stock> availableStocks = stockHandler.getAvailableStocks();
        for(Map.Entry<String, Stock > entry : availableStocks.entrySet()) {
            Stock stock = entry.getValue();
            message += "§7- §a" + stock.getDisplayName() + "\n";
        }
        message += "§9-----------------------------\n";
        p.sendMessage(message);
    }

    private void showHelp() {
        String message = "";
        message += "§9-----------------------------\n";
        message += "§cMcStocks Help§8:\n";
        message += "§7- §e/stock list §7- §6Shows all available stocks\n";
        message += "§7- §e/stock price <stock> §7- §6Shows the price of a given stock\n";
        message += "§7- §e/stock buy <name> <amount> §7- §6Buys X shares of a given stock\n";
        message += "§7- §e/stock portfolio §7- §6Opens your portfolio\n";
        message += "§7- §e/money §7- §6Shows your balance\n";
        message += "§7- §e/moneytop §7- §6Shows the top 5 richest players\n";
        message += "§7- §e/setmoney <player> <amount> §7- §6Sets the balance of the player to a given amount\n";
        message += "§7- §e/pay <amount> §7- §6Pays money to another player\n";
        message += "§9-----------------------------\n";
        p.sendMessage(message);
    }
}
