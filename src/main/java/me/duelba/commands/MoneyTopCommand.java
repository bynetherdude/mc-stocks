package me.duelba.commands;

import me.duelba.mysql.StockSQL;
import me.duelba.utils.FormatValueUtility;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MoneyTopCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = (Player) sender;
        FormatValueUtility format = new FormatValueUtility();
        ResultSet rs = StockSQL.getMoneyTop();
        String message = "§9-----------------------------\n";
        int rowCount = 0;
        try {
            rs.last();
            rowCount = rs.getRow();
            for (int i = 1; i <= rowCount; i++) {
                rs.absolute(i);
                message += "§a#" + i + " » §6" + rs.getString("username") + "§a » §6" + format.addThousandSeparator(rs.getDouble("balance")) + "$\n";
            }
            message += "§9-----------------------------\n";
            p.sendMessage(message);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
