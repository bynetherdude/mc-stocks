package me.duelba.events;

import me.duelba.gui.Confirm;
import me.duelba.gui.Portfolio;
import me.duelba.main.Main;
import me.duelba.stock.StockHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        StockHandler stockHandler = new StockHandler();

        Portfolio pf;
        if(!Main.getPortFolioList().containsKey(p.getUniqueId().toString())) {
            Main.getPortFolioList().put(p.getUniqueId().toString(), new Portfolio(p.getUniqueId().toString()));
        }
        pf = Main.getPortFolioList().get(p.getUniqueId().toString());

        Inventory inv = e.getInventory();

        if(!e.getCurrentItem().hasItemMeta()) {
            return;
        }

        if(inv.getName().equals("§6§lPortfolio")) {
            e.setCancelled(true);
            if(e.getClick().isLeftClick() && e.getCurrentItem().getType() == Material.BOOK) {
                ItemMeta itemMeta = e.getCurrentItem().getItemMeta();
                String id = itemMeta.getLore().get(4);
                Confirm confirm = new Confirm(id, "Confirm Sale");
                p.getOpenInventory().close();
                p.openInventory(confirm.showConfirmSaleInventory());
                return;
            }
            if(e.getClick().isRightClick() && e.getCurrentItem().getType() == Material.BOOK) {
                ItemMeta itemMeta = e.getCurrentItem().getItemMeta();
                String id = itemMeta.getLore().get(4);
                Confirm confirm = new Confirm(id, "Confirm Sale of ALL shares");
                p.getOpenInventory().close();
                p.openInventory(confirm.showConfirmSaleInventory());
                return;
            }
            if(e.getCurrentItem().getType() == Material.SKULL_ITEM && e.getCurrentItem().getItemMeta().getDisplayName().equals("§a««« Last page")) {

            }
            if(e.getCurrentItem().getType() == Material.SKULL_ITEM && e.getCurrentItem().getItemMeta().getDisplayName().equals("§aNext page »»»")) {
                pf.setCurrentPage(pf.getCurrentPage() + 1);
                Inventory inventory = pf.getInv(pf.getCurrentPage() * 17);
            }
        }
        if(inv.getName().equals("§6§lConfirm Sale")) {
            if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§aConfirm Sale")) {
                String id = e.getCurrentItem().getItemMeta().getLore().get(0);
                stockHandler.sellOneShare(id.replaceAll("§", ""), p);
                p.getOpenInventory().close();
                openPortfolio(p, pf);
                return;
            }
            if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§cAbort Sale")) {
                p.getOpenInventory().close();
                openPortfolio(p, pf);
                return;
            }
        }
        if(inv.getName().equals("§6§lConfirm Sale of ALL shares")) {
            if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§aConfirm Sale")) {
                String id = e.getCurrentItem().getItemMeta().getLore().get(0);
                stockHandler.sellAllShares(id.replaceAll("§", ""), p);
                p.getOpenInventory().close();
                openPortfolio(p, pf);
                return;
            }
            if(e.getCurrentItem().getItemMeta().getDisplayName().equals("§cAbort Sale")) {
                p.getOpenInventory().close();
                openPortfolio(p, pf);
            }
        }
    }

    @EventHandler
    public void onInteract(InventoryInteractEvent e) {
        Inventory inv = e.getInventory();
        if(inv.getName().equals("§6§lPortfolio")) {
            e.setCancelled(true);
        }
    }

    private void openPortfolio(Player p, Portfolio pf) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.getPlugin(Main.class), new Runnable() {
            @Override
            public void run() {
                p.openInventory(pf.getInv(pf.getCurrentPage() * 17));
            }
        }, 1);
    }
}
